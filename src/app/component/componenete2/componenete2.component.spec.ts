import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Componenete2Component } from './componenete2.component';

describe('Componenete2Component', () => {
  let component: Componenete2Component;
  let fixture: ComponentFixture<Componenete2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Componenete2Component ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(Componenete2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
